from pathlib import Path
import os

DATABASES = {
    'default': {
        'database': 'skunk',
    }
}
RECIPES = [Path(__file__).parent / 'recipe.yml']
DEBUG = bool(os.environ.get('DEBUG', False))
if DEBUG:
    MAX_AGE = False  # Deactivate cache control while developping.
