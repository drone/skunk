# Position conformity

`https://drone.api.gouv.fr/flightareas/check`

## Required GET parameters

* `lat`: a latitude in WGS84 format.
* `lon`: a longitude in WGS84 format.
* `alt`: an altitude in meters.

## Response

```json
{
    "allowed": true/false/null,
    "ceiling": <value in meters>,
    "areas": <list of labels/description>
}
```

## Example

`https://drone.api.gouv.fr/flightareas/check?lat=43.51&lon=5.35&alt=0`

```json
{
    "allowed": null,
    "ceiling": 0,
    "areas": [
        {
            "description": "MON-FRI: 0600- 1600. SUM:-1HR. Activity known on Salon APP (FREQ Transit VFR).",
            "label": "D 155"
        },
        {
            "description": "Activable H24",
            "label": "R 95 A"
        }
    ]
}
```

## Optional GET parameters

### `allowed`

A `true` allowed position is conform, a `false` one is in a forbidden
area and `null` indicates a point which requires a manual check
by the pilot.

### `ceiling`

The ceiling in meters from which the allowance is effective.

### `areas`

A list of label/description for areas related to that point.
The description gives some insights, especially for unknown statuses.

