import asyncio
import os

import ujson as json
import uvloop
from asyncpg import create_pool
from postgis import Point
from postgis.asyncpg import register
from roll import Roll
from roll.extensions import cors, simple_server

DB_CONFIG = {
    'database': os.environ.get('POSTGRES_DB', 'skunk'),
    'host': os.environ.get('POSTGRES_HOST', None),
    'user': os.environ.get('POSTGRES_USER', None),
    'password': os.environ.get('POSTGRES_PASSWORD', None),
}


asyncio.set_event_loop(uvloop.new_event_loop())
app = Roll()
cors(app)


@app.listen('startup')
async def register_db():
    app.pool = await create_pool(**DB_CONFIG, max_size=100, loop=app.loop,
                                 init=register)


@app.listen('shutdown')
async def close_connection():
    await app.pool.close()


@app.route('/check/')
async def check(request, response):
    lon = request.query.float('lon')
    lat = request.query.float('lat')
    request.query.float('alt')  # Required but not in use yet.
    position = Point(lon, lat, srid=4326)
    async with app.pool.acquire() as conn:
        query = '''
            SELECT
                forbidden, floor, properties
            FROM
                areas
            WHERE
                ST_Intersects(shape, $1)
        '''
        records = await conn.fetch(query, position)
    if not records:
        response.json = {'allowed': True}
        return
    statuses = []
    ceiling = 9000
    areas = []
    labels = []
    for record in records:
        is_forbidden = record['forbidden']
        statuses.append(is_forbidden)
        ceiling = min(record['floor'], ceiling)
        properties = json.loads(record['properties'])
        label = properties['label']
        # Dedupe labels except for unknown areas.
        if label in labels and is_forbidden is not None:
            continue
        areas.append(properties)
        labels.append(label)
    # Priority upon explicitly allowed areas.
    if False in statuses:
        allowed = True
    elif True in statuses:  # Priority upon unknown areas.
        allowed = False
    elif None in statuses:
        allowed = None
    response.json = {'allowed': allowed, 'ceiling': ceiling, 'areas': areas}


if __name__ == '__main__':
    simple_server(app, host='127.0.0.1', port=9393)
