# Skunk

Validate drone positions.

For usage as a consumer of APIs, check https://drone.beta.gouv.fr/developers/


## Development

* create a virtualenv and install `requirements-dev.txt`
* create a postgresql database named `skunk`
* run `python cli.py initdb`
* run `python server.py`
* `http :9393/check/ lat==0 lon==0 alt==0` should return a `{"allowed": true}`


## Running Utilery

* set the `UTILERY_SETTINGS` env var, for instance with fish:
  `set -gx UTILERY_SETTINGS /absolute/path/to/skunk/utilery/settings.py`
* run the app with gunicorn:
  `gunicorn utilery.views:app -b 0.0.0.0:3579 -c utilery/gunicorn.conf`
* `http :3579/all/12/2063/1407/geojson` should return data once `skunk`
  data base has been filled.
