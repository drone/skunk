import asyncio
import re
from pathlib import Path

import asyncpg
import ujson as json
from lxml import etree
from minicli import cli, run
from postgis import Point, Polygon, LineString
from postgis.asyncpg import register
from server import DB_CONFIG


def ft2m(ft):
    return int(ft * 0.3048)


@cli
async def initdb():
    conn = await asyncpg.connect(**DB_CONFIG)
    await conn.execute('CREATE EXTENSION IF NOT EXISTS postgis')
    await conn.execute('''
        CREATE TABLE IF NOT EXISTS areas (
            id SERIAL PRIMARY KEY,
            shape geography(Polygon),
            floor smallint NOT NULL DEFAULT 0,
            forbidden boolean NULL DEFAULT true,
            properties jsonb
        )
    ''')
    await conn.execute(
        'CREATE INDEX IF NOT EXISTS areas_idx ON areas USING GIST (shape)')
    print('Table areas initialized')


@cli
async def resetdb():
    conn = await asyncpg.connect(**DB_CONFIG)
    await conn.execute('TRUNCATE table areas')
    print('Table areas truncated')


@cli
async def import_geojson(filepath):

    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    with Path(filepath).open() as f:
        data = json.loads(f.read())
    for feature in data['features']:
        if feature['geometry']['type'] != 'Polygon':
            continue
        shape = Polygon(feature['geometry']['coordinates'], srid=4326)
        count = await conn.fetchval(
            'SELECT count(*) FROM areas '
            'WHERE ST_Within($1::geometry, shape::geometry)', shape)
        if count:
            continue
        await insert_area(conn, shape=shape, **feature['properties'])
    await conn.close()


lonlat_pattern = re.compile(
    r'(?P<lat_deg>\d{2})°(?P<lat_min>\d{2})\'(?P<lat_sec>\d{2})"N , '
    '(?P<lon_deg>\d{3})°(?P<lon_min>\d{2})\'(?P<lon_sec>\d{2})"(?P<side>[EW])')


def degminsec_to_lonlat(s):
    search = lonlat_pattern.search(s)
    if search:
        res = search.groupdict()
        lon = (float(res['lon_deg']) +
               float(res['lon_min']) / 60 + float(res['lon_sec']) / 3600)
        if res['side'] == 'W':
            lon = lon * -1.0
        lat = (float(res['lat_deg']) +
               float(res['lat_min']) / 60 + float(res['lat_sec']) / 3600)
        return Point(lon, lat, srid=4326)


FORBIDDEN_CTR = [
    'AVORD', 'BRICY', 'CHATEAUDUN', 'COGNAC', 'ETAIN', 'EVREUX', 'HYERES',
    'ISTRES', 'LANDIVISIAU', 'LANVEOC', 'LORIENT', 'LUXEUIL', 'MARSAN',
    'OCHEY', 'ORANGE', 'SAINT-DIZIER', 'TOURS VAL DE LOIRE', 'VILLACOUBLAY'
]
FORBIDDEN_SPACE_TYPES = ('CTR', 'S/CTR', 'Bal', 'D', 'P', 'ZIT', 'R')
AUTHORIZED_SPACE_TYPES = ('Aer',)


def floor_from_volumes(root, pk):
    volumes = root.xpath('Volume[PlancherRefUnite="SFC" '
                         'or PlancherRefUnite[starts-with(., "ft")]]'
                         '/Partie[@pk={}]/parent::Volume'.format(pk))
    # No volume means only FL floor and ceil.
    # More than one volume are always too high, from our experience.
    if not volumes or len(volumes) > 1:
        return None, None
    volume = volumes[0]
    hor_code = volume.find('HorCode').text
    hor_txt = (volume.find('HorTxt').text
               if volume.find('HorTxt') is not None
               else '')
    # H24 is full-time, SR-SS means from SunRise to SunSet.
    # TODO: deal with exceptions in H24/HJ hor_text.
    if (hor_code in ('H24', 'HJ', 'HJ+') or
            hor_txt.startswith(('H24', 'SR-SS'))):
        is_transient = False
    else:
        is_transient = hor_txt
    unit = volume.find('PlancherRefUnite').text
    ft_floor = int(volume.find('Plancher').text)
    if unit == 'ft AMSL':  # Above Medium Sea Level.
        # TODO: We should filter using DEM when available.
        floor = ft2m(ft_floor)
    elif unit == 'ft ASFC':  # Above Surface.
        floor = ft2m(ft_floor)
    else:
        floor = 0  # SFC / Surface.
    if floor == 152:
        # 152 means 500 ft, and the aeronautical convention is that
        # 500 ft == 150 m.
        floor = 150
    return floor, is_transient


async def shape_from_part(conn, part):
    coords = part.find('Geometrie').text
    coords = [list(map(float, c.split(',')))[::-1]
              for c in coords.split('\n')]
    if len(coords) == 1:
        # Single coordinate, mainly aeg type, let's create a symbolic
        # area around.
        # TODO: a lot of this single point geometries are "Aeromodelisme"
        # areas, we need to find the real shapes in other sources.
        point = Point(*coords)
        shape = await conn.fetchval('SELECT ST_Buffer($1::geography, 500)',
                                    point)
    elif coords[0] != coords[-1]:
            # We have a least one line case in the data,
            # which is a high velocity way, so we better keep it.
            # TODO: define an "official" buffer around the line.
            line = LineString(coords)
            shape = await conn.fetchval('SELECT ST_Buffer($1::geography, 500)',
                                        line)
    else:
        shape = Polygon([coords])
    return shape


async def insert_area(conn, shape, floor=0, forbidden=True, **properties):
    await conn.execute(
        'INSERT INTO areas (shape, floor, forbidden, properties) '
        'VALUES($1, $2, $3, $4)',
        shape, floor, forbidden, json.dumps(properties))


async def import_spaces(conn, root):
    parts = root.findall('Situation/PartieS/Partie')
    spaces = root.find('Situation/EspaceS')
    volumes = root.find('Situation/VolumeS')
    for part in parts:
        pk = part.get('pk')
        space_pk = part.find('Espace').get('pk')
        space = spaces.xpath('Espace[@pk={}]'.format(space_pk))[0]
        type_ = space.find('TypeEspace').text
        if type_ not in FORBIDDEN_SPACE_TYPES + AUTHORIZED_SPACE_TYPES:
            continue
        floor, is_transient = floor_from_volumes(volumes, pk)
        if floor is None or floor >= 150:
            continue
        name = space.find('Nom').text
        if type_ == 'CTR':
            floor = 0 if name in FORBIDDEN_CTR else 50
        forbidden = type_ not in AUTHORIZED_SPACE_TYPES
        if forbidden and is_transient is not False:
            forbidden = None
        shape = await shape_from_part(conn, part)
        label = '{} {}'.format(type_, name)
        await insert_area(conn, shape, floor, forbidden, label=label,
                          description=is_transient or '')


async def import_helistations(conn, root):
    helistations = root.findall('Situation/HelistationS/Helistation')
    for helistation in helistations:
        coords = helistation.find('Geometrie').text
        coords = list(map(float, coords.split(',')))[::-1]
        point = Point(*coords)
        label = helistation.find('Nom').text
        shape = await conn.fetchval('SELECT ST_Buffer($1::geography, 1000)',
                                    point)
        await insert_area(conn, shape, label=label)
        shape = await conn.fetchval('SELECT ST_Buffer($1::geography, 2500)',
                                    point)
        await insert_area(conn, shape, floor=50, label=label)
        shape = await conn.fetchval('SELECT ST_Buffer($1::geography, 3500)',
                                    point)
        await insert_area(conn, shape, floor=100, label=label)


async def create_short_runway_shape(conn, p1, p2, width):
    # Measures are from the runway center.
    return await conn.fetchval('''
        SELECT
            ST_MakePolygon(ST_MakeLine(ARRAY[
                ST_Project(pp1, $4, az1 + radians(90))::geometry,
                ST_Project(pp1, $4, az1 - radians(90))::geometry,
                ST_Project(pp2, $4, az1 - radians(90))::geometry,
                ST_Project(pp2, $4, az1 + radians(90))::geometry,
                ST_Project(pp1, $4, az1 + radians(90))::geometry
                ]))
        FROM (
            SELECT
                ST_Project(center, $3, az1) as pp1,
                ST_Project(center, $3, az2) as pp2, az1, az2
                FROM (
                    SELECT
                        ST_Azimuth($1::geography, $2::geography) AS az1,
                        ST_Azimuth($2::geography, $1::geography) AS az2,
                        ST_Centroid(ST_Collect($1::geometry, $2::geometry))
                            AS center
                ) AS sub2
        ) AS sub
    ''', p1, p2, 5000, width)


async def create_long_runway_shape(conn, p1, p2, width):
    # Measures are from both runway edges.
    return await conn.fetchval('''
        SELECT
            ST_MakePolygon(ST_MakeLine(ARRAY[
                ST_Project(pp1, $4, az1 + radians(90))::geometry,
                ST_Project(pp1, $4, az1 - radians(90))::geometry,
                ST_Project(pp2, $4, az1 - radians(90))::geometry,
                ST_Project(pp2, $4, az1 + radians(90))::geometry,
                ST_Project(pp1, $4, az1 + radians(90))::geometry
                ]))
        FROM (
            SELECT
                ST_Project($1, $3, az1) as pp1,
                ST_Project($2, $3, az2) as pp2, az1, az2
                FROM (
                    SELECT
                        ST_Azimuth($1::geography, $2::geography) AS az1,
                        ST_Azimuth($2::geography, $1::geography) AS az2
                ) AS sub2
        ) AS sub
    ''', p1, p2, 10000, width)


async def import_runways(conn, root):
    runways = root.findall('Situation/RwyS/Rwy')
    for runway in runways:
        label = runway.get('lk')
        if runway.find('LatThr1') is None or runway.find('LatThr2') is None:
            continue
        ctr = None
        ad_node = runway.find('Ad')
        if ad_node is not None:
            ad_pk = ad_node.get('pk')
            ad = root.xpath('Situation/AdS/Ad[@pk={}]'.format(ad_pk))[0]
            ctr = ad.find('Ctr')
        lat1 = float(runway.find('LatThr1').text)
        lon1 = float(runway.find('LongThr1').text)
        lat2 = float(runway.find('LatThr2').text)
        lon2 = float(runway.find('LongThr2').text)
        p1 = Point([lon1, lat1])
        p2 = Point([lon2, lat2])
        length = int(runway.find('Longueur').text)
        # TODO: law says "IFR ready runway", we use "runway in a ctr", make
        # sure this assumption is good.
        if length > 1200 or ctr is not None:
            shape = await create_long_runway_shape(conn, p1, p2, 2500)
            await insert_area(conn, shape, label=label)
            shape = await create_long_runway_shape(conn, p1, p2, 5000)
            await insert_area(conn, shape, floor=30, label=label)
            if ctr is None:  # When there is a ctr, its shape prevails.
                shape = await create_long_runway_shape(conn, p1, p2, 8000)
                await insert_area(conn, shape, floor=60, label=label)
                shape = await create_long_runway_shape(conn, p1, p2, 10000)
                await insert_area(conn, shape, floor=100, label=label)
        else:
            shape = await create_short_runway_shape(conn, p1, p2, 500)
            await insert_area(conn, shape, label=label)
            shape = await create_short_runway_shape(conn, p1, p2, 3500)
            await insert_area(conn, shape, floor=50, label=label)
            shape = await create_short_runway_shape(conn, p1, p2, 5000)
            await insert_area(conn, shape, floor=100, label=label)


@cli
async def parse_sia_xml(filepath):
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    root = etree.parse(filepath).getroot()
    await import_spaces(conn, root)
    await import_helistations(conn, root)
    await import_runways(conn, root)
    await conn.close()


if __name__ == '__main__':
    run()
