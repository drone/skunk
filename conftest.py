import asyncio

import asyncpg
import pytest
import ujson as json
from cli import initdb
from postgis import Polygon
from postgis.asyncpg import register
from server import app as myapp
from server import DB_CONFIG

DB_CONFIG['database'] = 'test_skunk'


def pytest_configure(config):
    asyncio.get_event_loop().run_until_complete(initdb())


def pytest_runtest_setup():

    async def main():
        conn = await asyncpg.connect(**DB_CONFIG)
        await conn.execute('TRUNCATE areas;')
        await conn.close()

    asyncio.get_event_loop().run_until_complete(main())


@pytest.yield_fixture
async def connection():
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    yield conn


@pytest.yield_fixture
async def area(connection):
    async def create_area(floor=0, forbidden=True, **properties):
        if not properties:
            properties = {'label': 'default'}
        polygon = Polygon([[[4, 48], [5, 48], [5, 49], [4, 49], [4, 48]]])
        await connection.execute(
            'INSERT INTO "areas" (shape, floor, forbidden, properties) '
            'VALUES ($1, $2, $3, $4)',
            polygon, floor, forbidden, json.dumps(properties))
    yield create_area


@pytest.fixture
def app():
    return myapp
