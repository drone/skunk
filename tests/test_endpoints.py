import json
from http import HTTPStatus

import pytest

pytestmark = pytest.mark.asyncio


async def test_returns_true_outside_forbidden_area(client, area, app):
    await area()
    resp = await client.get('/check/?lon=8&lat=50&alt=10')
    assert resp.status == HTTPStatus.OK
    assert resp.headers['Access-Control-Allow-Origin'] == '*'
    assert json.loads(resp.body) == {'allowed': True}


async def test_returns_false_inside_forbidden_area(client, area, app):
    await area()
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body) == {
        'allowed': False,
        'ceiling': 0,
        'areas': [{'label': 'default'}]
    }


async def test_returns_none_inside_unknown_area(client, area, app):
    await area(forbidden=None, label='foo')
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body) == {
        'allowed': None,
        'ceiling': 0,
        'areas': [{'label': 'foo'}]
    }


async def test_returns_none_and_desc_inside_unknown_area(client, area, app):
    await area(forbidden=None, label='foo', description='bar')
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body) == {
        'allowed': None,
        'ceiling': 0,
        'areas': [{'label': 'foo', 'description': 'bar'}]
    }


async def test_returns_none_and_descs_inside_unknown_area(client, area, app):
    await area(forbidden=None, label='foo', description='bar')
    await area(forbidden=None, label='baz', description='quux')
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body) == {
        'allowed': None,
        'ceiling': 0,
        'areas': [
            {'label': 'foo', 'description': 'bar'},
            {'label': 'baz', 'description': 'quux'}
        ]
    }


async def test_not_forbidden_should_prevail_forbidden_one(client, area, app):
    await area()
    await area(forbidden=False)
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['allowed'] is True


async def test_not_forbidden_should_prevail_unknown_one(client, area, app):
    await area(forbidden=None)
    await area(forbidden=False)
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['allowed'] is True


async def test_forbidden_should_prevail_unknown_one(client, area, app):
    await area()
    await area(forbidden=None)
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['allowed'] is False


async def test_forbidden_labels_should_be_deduped(client, area, app):
    await area(label='foo')
    await area(label='foo')
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['areas'] == [{'label': 'foo'}]


async def test_unknown_labels_should_not_be_deduped(client, area, app):
    await area(label='foo', description='bar1', forbidden=None)
    await area(label='foo', description='bar2', forbidden=None)
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['areas'] == [
        {'label': 'foo', 'description': 'bar1'},
        {'label': 'foo', 'description': 'bar2'}
    ]


async def test_allowed_labels_should_be_deduped(client, area, app):
    await area(label='foo', forbidden=False)
    await area(label='foo', forbidden=False)
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['areas'] == [{'label': 'foo'}]


async def test_minimal_ceiling_should_be_returned(client, area, app):
    await area(floor=100, label='foo')
    await area(floor=50, label='bar')
    resp = await client.get('/check/?lon=4.5&lat=48.5&alt=11')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['ceiling'] == 50


async def test_position_with_alt_0_should_pass(client, area, app):
    await area()
    resp = await client.get('/check/?lon=8&lat=50&alt=0')
    assert resp.status == HTTPStatus.OK
    assert json.loads(resp.body)['allowed'] is True


async def test_missing_lat_or_lon_or_alt(client, area, app):
    await area()
    resp = await client.get('/check/?lon=4.5&alt=10')
    assert resp.status == HTTPStatus.BAD_REQUEST
    resp = await client.get('/check/?lat=4.5&alt=10')
    assert resp.status == HTTPStatus.BAD_REQUEST
    resp = await client.get('/check/?lat=4.5&lon=10')
    assert resp.status == HTTPStatus.BAD_REQUEST
