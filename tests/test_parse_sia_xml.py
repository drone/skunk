import json
from pathlib import Path

import pytest

from cli import parse_sia_xml

pytestmark = pytest.mark.asyncio


async def test_import_P_area(connection):

    path = Path(__file__).parent / 'data/sia/p_area.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas')
    assert len(res) == 1
    row = res[0]
    assert row['floor'] == 0
    assert row['forbidden'] is True


async def test_import_Aer_area(connection):

    path = Path(__file__).parent / 'data/sia/aer_area.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas')
    assert len(res) == 1
    row = res[0]
    assert row['floor'] == 0
    assert row['forbidden'] is False


async def test_import_CTR_area(connection):

    path = Path(__file__).parent / 'data/sia/ctr_area.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas')
    assert len(res) == 1
    row = res[0]
    assert row['floor'] == 50
    assert row['forbidden'] is None
    properties = json.loads(row['properties'])
    assert properties['label'] == 'CTR ANGERS'
    assert properties['description'] == 'TUE-SAT: 0700 - 1800 ( SUM -1H ).'


async def test_import_CTR_area_h24(connection):

    path = Path(__file__).parent / 'data/sia/ctr_area_h24.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas')
    assert len(res) == 1
    row = res[0]
    assert row['floor'] == 50
    assert row['forbidden'] is True


async def test_import_Bal_area_without_hortxt_should_not_fail(connection):

    path = Path(__file__).parent / 'data/sia/bal_area_without_hortxt.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas')
    assert len(res) == 1
    row = res[0]
    assert row['floor'] == 0
    assert row['forbidden'] is None
    properties = json.loads(row['properties'])
    assert properties['label'] == 'Bal 976'
    assert properties['description'] == ''


async def test_import_forbidden_CTR_area(connection):

    path = Path(__file__).parent / 'data/sia/forbidden_ctr_area.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas')
    assert len(res) == 1
    row = res[0]
    assert row['floor'] == 0
    assert row['forbidden'] is None
    properties = json.loads(row['properties'])
    assert properties['label'] == 'CTR CHATEAUDUN'
    assert properties['description'] == 'Possible activation H24.'


async def test_import_forbidden_CTR_area_hj(connection):

    path = Path(__file__).parent / 'data/sia/forbidden_ctr_area_hj.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas')
    assert len(res) == 1
    row = res[0]
    assert row['floor'] == 0
    assert row['forbidden'] is True


async def test_import_too_high_R_area(connection):

    path = Path(__file__).parent / 'data/sia/too_high_area.xml'
    await parse_sia_xml(str(path))

    assert not await connection.fetch('SELECT * FROM areas')


async def test_import_helistation(connection):

    path = Path(__file__).parent / 'data/sia/helistation_area.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas ORDER BY floor')
    assert len(res) == 3
    assert res[0]['floor'] == 0
    assert res[0]['forbidden'] is True
    assert res[1]['floor'] == 50
    assert res[1]['forbidden'] is True
    assert res[2]['floor'] == 100
    assert res[2]['forbidden'] is True


async def test_import_short_runway(connection):

    path = Path(__file__).parent / 'data/sia/short_runway.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas ORDER BY floor')
    assert len(res) == 3
    assert res[0]['floor'] == 0
    assert res[0]['forbidden'] is True
    assert res[1]['floor'] == 50
    assert res[1]['forbidden'] is True
    assert res[2]['floor'] == 100
    assert res[2]['forbidden'] is True


async def test_import_long_runway(connection):

    path = Path(__file__).parent / 'data/sia/long_runway.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas ORDER BY floor')
    assert len(res) == 4
    assert res[0]['floor'] == 0
    assert res[0]['forbidden'] is True
    assert res[1]['floor'] == 30
    assert res[1]['forbidden'] is True
    assert res[2]['floor'] == 60
    assert res[2]['forbidden'] is True
    assert res[3]['floor'] == 100
    assert res[3]['forbidden'] is True


async def test_import_long_runway_with_ctr(connection):

    path = Path(__file__).parent / 'data/sia/long_runway_with_ctr.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas ORDER BY floor')
    assert len(res) == 2
    assert res[0]['floor'] == 0
    assert res[0]['forbidden'] is True
    assert res[1]['floor'] == 30
    assert res[1]['forbidden'] is True


async def test_import_short_runway_with_ctr(connection):

    path = Path(__file__).parent / 'data/sia/short_runway_with_ctr.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas ORDER BY floor')
    assert len(res) == 2
    assert res[0]['floor'] == 0
    assert res[0]['forbidden'] is True
    assert res[1]['floor'] == 30
    assert res[1]['forbidden'] is True


async def test_should_not_import_if_floor_is_500ft(connection):

    path = Path(__file__).parent / 'data/sia/r_area_at_500ft_floor.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas ORDER BY floor')
    assert len(res) == 0


async def test_import_non_closed_shape(connection):

    path = Path(__file__).parent / 'data/sia/non_closed_shape.xml'
    await parse_sia_xml(str(path))

    res = await connection.fetch('SELECT * FROM areas ORDER BY floor')
    assert len(res) == 1
    # Should be a valid Polygon.
    coords = res[0]['shape'].coords[0]
    assert len(coords) > 2
    assert coords[0] == coords[-1]
